#include <String.h>
#include <iostream>

bool extractCypherkey(BString *out, const BString &url) {
  if (!url.StartsWith("ssb://")) {
    *out = "Not an ssb URL";
    return false;
  }
  if (url.Length() != 58 || url[6] != '%' || !url.EndsWith("=.sha256")) {
    *out = "Invalid cypherkey in URL";
    return false;
  }
  for (int i = 7; i < 50; i++) {
    char c = url[i];
    if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') ||
          (c >= '0' && c <= '9') || c == '+' || c == '/')) {
      *out = "Invalid cypherkey in URL";
      return false;
    }
  }
  url.CopyInto(*out, 6, 53);
  return true;
}

int main(int argc, char **args) {
  if (argc != 3) {
    std::cerr << "Usage: git-remote-ssb <name> <url>" << std::endl;
    return -1;
  }
  BString name(args[1]);
  BString url(args[2]);
  BString repoKey;
  if (!extractCypherkey(&repoKey, url)) {
    std::cerr << repoKey.String() << std::endl;
    return -1;
  }
  std::cerr << "Cypherkey = " << repoKey.String() << std::endl;
  return 0;
}
